const db = require('../utils/database'),
  sequelize = db.sequelize,
  Sequelize = db.Sequelize;

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');



const User = sequelize.define('userdata',
  {
    userid: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    username: {
      type: Sequelize.STRING,
      allowNull: false
    },
    pw: {
      type: Sequelize.STRING,
      allowNull: false
    },
  },
  {
    hooks: {
      beforeCreate: async function (user) {
        const salt = await bcrypt.genSalt(10);
        user.pw = await bcrypt.hash(user.pw, salt);
      }
    },
    defaultScope: {
      attributes: { exclude: ['pw'] }
    },
    scopes: {
      withPassword: {
        attributes: { include: ['pw'] }
      }
    }
  });

User.prototype.validatePassword = async function (password) {
  const valid = await bcrypt.compare(password, this.pw);
  return valid;
};

User.prototype.generateAuthToken = function () {

  const token = jwt.sign({ id: this.userid }, 'MySuperSecretKey!!11', {
    expiresIn: 86400 // expires in 24 hours
  });
  return({ auth: true, token: token });
};

module.exports = User;
