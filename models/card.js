const db = require('../utils/database'),
  sequelize = db.sequelize,
  Sequelize = db.Sequelize;

const Card = sequelize.define('card', {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true
  },
  idProduct: {
    type: Sequelize.INTEGER,
    allowNull: false,
  },

});

module.exports = Card;