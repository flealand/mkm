const User = require('../models/user');
const jwt = require('jsonwebtoken');

exports.login = (req, res, next) => {
  User.scope('withPassword').findOne({ where: { username: req.body.username } }).then(async function (user) {
    if (!user) {
      res.sendStatus(404);
    } else if (user.validatePassword(req.body.pw)) {
      res.send(user.generateAuthToken());
    } else {
      res.sendStatus(404);
    }
  });
};

exports.logout = (req, res, next) => {

};

exports.checkToken = (req, res, next) => {
  const authHeader = req.headers.authorization;

  if (authHeader) {
    const token = authHeader.replace('Bearer ', '');

    jwt.verify(token, 'MySuperSecretKey!!11', (err, user) => {
      if (err) {
        return res.sendStatus(403);
      }

      req.user = user;
      res.send(user);
    });
  } else {
    res.sendStatus(401);
  }
};

exports.verifyToken = (req, res, next) => {
  const authHeader = req.headers.authorization;

  if (authHeader) {
    const token = authHeader.replace('Bearer ', '');

    jwt.verify(token, 'MySuperSecretKey!!11', (err, user) => {
      if (err) {
        return res.sendStatus(403);
      }

      req.user = user;
      req.authenticated = true;
      next()
    });
  } else {
    req.user = null;
    req.authenticated =  false;
    // res.sendStatus(401);
    next();
  }
};
