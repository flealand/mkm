import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuardService} from './auth-guard.service';
import {LoginComponent} from './login/login.component';
import {SearchCardsComponent} from './search-cards/search-cards.component';
import {TrackedCardsComponent} from './tracked-cards/tracked-cards.component';


const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: '', component: LoginComponent },
  { path: 'search', component: SearchCardsComponent, canActivate: [AuthGuardService] },
  { path: 'manage', component: TrackedCardsComponent, canActivate: [AuthGuardService] },
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
