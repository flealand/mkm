import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrackedCardsComponent } from './tracked-cards.component';

describe('TrackedCardsComponent', () => {
  let component: TrackedCardsComponent;
  let fixture: ComponentFixture<TrackedCardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrackedCardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrackedCardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
