import {AfterViewInit, Component, OnInit} from '@angular/core';
import {Constants} from '../constants';
import {MkmService} from '../mkm.service';
import {Card} from '../models/card';

@Component({
  selector: 'app-tracked-cards',
  templateUrl: './tracked-cards.component.html',
  styleUrls: ['./tracked-cards.component.scss']
})
export class TrackedCardsComponent implements OnInit, AfterViewInit {

  cards: any = [];

  constructor(private mkmService: MkmService) { }

  ngOnInit() {
  }

  ngAfterViewInit(): void {

    this.mkmService.getTrackedCards().subscribe(res => {
      this.cards = [];

        for (const card of res) {
          let tmpCard: Card = {
            product: {
              idProduct: card.product.idProduct,
              name: card.product.name[Constants.ENG_ID].productName,
              image: card.product.image,
              expansion: card.product.expansion,
              rarity: card.product.rarity,
              priceGuide: card.product.priceGuide,
              isTracked: true,
            }
          };
          this.cards.push(tmpCard);
        }

    });

  }

}
