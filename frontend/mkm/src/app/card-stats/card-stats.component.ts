import {AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {Card} from '../models/card';

@Component({
  selector: 'app-card-stats',
  templateUrl: './card-stats.component.html',
  styleUrls: ['./card-stats.component.scss']
})
export class CardStatsComponent implements OnInit, AfterViewInit, OnChanges{

  sumOfAllTimeAvgs: string = '0';
  avgOfFoundArticles: string = '0';

  @Input()
  cards: Card[];

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit(): void {

  }

  ngOnChanges(changes: SimpleChanges): void {

    if (changes.cards) {
      let sumOfAvgs: number = 0;

      for (const card of this.cards) {
        if (!card.product.priceGuide) {
          return;
        }
        sumOfAvgs += card.product.priceGuide.AVG;
      }

      if (this.cards.length > 0) {
        this.avgOfFoundArticles = (sumOfAvgs / this.cards.length).toFixed(2);
        this.sumOfAllTimeAvgs = sumOfAvgs.toFixed(2);
      }
    }

  }
}
