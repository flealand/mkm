import { TestBed } from '@angular/core/testing';

import { MkmService } from './mkm.service';

describe('MkmService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MkmService = TestBed.get(MkmService);
    expect(service).toBeTruthy();
  });
});
