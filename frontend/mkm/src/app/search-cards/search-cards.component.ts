import {Component, OnInit, QueryList, ViewChildren} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {CardComponent} from '../card/card.component';
import {Constants} from '../constants';
import {MkmService} from '../mkm.service';
import {Card} from '../models/card';

@Component({
  selector: 'app-search-cards',
  templateUrl: './search-cards.component.html',
  styleUrls: ['./search-cards.component.scss']
})
export class SearchCardsComponent implements OnInit {
  expansions: any = [];

  @ViewChildren(CardComponent) cardComponents: QueryList<CardComponent>;

  searchForm: FormGroup;

  cards: any = [];

  constructor(private mkmService: MkmService, private fb: FormBuilder) {
    this.searchForm = this.createFormGroup();
  }

  ngOnInit() {
    this.getAvailableExpansions();
  }

  private getAvailableExpansions(): void {
    this.mkmService.getExpansions().subscribe(expos => {

      for (const exp of expos.expansion) {
        this.expansions.push(exp);
      }

      this.expansions = this.expansions.sort(function (a, b) {
        return a.enName - b.enName;
      });
    });
  }



  searchCardsFromMkm(cardName: string) {

    this.mkmService.searchForCards(cardName).subscribe(res => {
      this.cards = [];

        for (const card of res.product) {
          let tmpCard: Card = {
            product: {
              idProduct: card.idProduct,
              name: card.name[Constants.ENG_ID].productName,
              image: card.image,
              expansion: card.expansion,
              rarity: card.rarity,
              priceGuide: card.priceGuide,
              isTracked: card.isTracked,
              website: card.website,
            }
          };
          this.cards.push(tmpCard);
        }
    });
  }

  private createFormGroup(): FormGroup {
    return this.fb.group({
      cardName: ['', Validators.minLength(4)],
    });
  }

  onSubmit(formValues: any) {
    if (formValues.status && formValues.status === 'VALID') {
      this.searchCardsFromMkm(formValues.value.cardName)
    }
  }

  expansionSeleted(expansion) {
    this.mkmService.getCardsFromExpansion(expansion.value).subscribe(cards => {
      this.cards = [];

      for (const card of cards.single) {
        let tmpCard: Card = {
          product: {
            idProduct: card.idProduct,
            name: card.enName,
            image: card.image,
            expansion: card.expansion,
            rarity: card.rarity,
            // priceGuide: card.priceGuide,
            isTracked: card.isTracked,
            website: card.website,
          }
        };
        this.cards.push(tmpCard);
      }
    });
  }
}
