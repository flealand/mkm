import {HttpClient, HttpHeaders} from '@angular/common/http';
import {placeholdersToParams} from '@angular/compiler/src/render3/view/i18n/util';
import { Injectable } from '@angular/core';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {AuthService} from './auth.service';
import {Card} from './models/card';

@Injectable({
  providedIn: 'root'
})
export class MkmService {

  public static readonly BASE_URL: string = 'http://deapps.ddns.net:3000/';

  constructor(private http: HttpClient, private authService: AuthService) {
  }

  public searchForCards(cardName: string): Observable<any> {

    return this.http.get(MkmService.BASE_URL + 'cards/search', {
      params: {
      name : cardName.replace(' ', '&'),
      },
      headers: {Authorization: this.authService.getToken()}
    }).pipe(
      map(this.extractData));
  }

  public getExpansions(): Observable<any> {
    return this.http.get(MkmService.BASE_URL + 'cards/expansion', {
      headers: {Authorization: this.authService.getToken()}
    }).pipe(
      map(this.extractData));
  }

  public getTrackedCards(): Observable<any> {
    return this.http.get(MkmService.BASE_URL + 'cards/tracked',
      {headers: {Authorization: this.authService.getToken()}}
    ).pipe(
      map(this.extractData));
  }

  public getCardsFromExpansion(id: string): Observable<any> {

    return this.http.get(MkmService.BASE_URL + 'cards/expansion/specific', {
      params: {
        id : id,
      },
      headers: {Authorization: this.authService.getToken()}
    }).pipe(
      map(this.extractData));
  }

  public getCardsById(id: string): Observable<any> {

    return this.http.get(MkmService.BASE_URL + 'cards/id', {
      params: {
        id : id,
      },
      headers: {Authorization: this.authService.getToken()}
    }).pipe(
      map(this.extractData));
  }

  public saveCardsToDataBase(card: Card):Observable<any> {
    return this.http.post(MkmService.BASE_URL + 'cards/save', card,
      {headers: {Authorization: this.authService.getToken()}}
      );
  }

  private extractData(res: Response) {
    let body = res;
    return body || {
    };
  }

}
