import {HttpClient, HttpClientModule, HttpHandler} from '@angular/common/http';
import {FlexLayoutModule} from '@angular/flex-layout';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatMenuModule} from '@angular/material/menu';
import {MatSelectModule} from '@angular/material/select';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatToolbarModule} from '@angular/material/toolbar';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CardComponent } from './card/card.component';
import { PriceGuideComponent } from './card/price-guide/price-guide.component';
import {LoginComponent} from './login/login.component';
import { SearchCardsComponent } from './search-cards/search-cards.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TrackedCardsComponent } from './tracked-cards/tracked-cards.component';
import { CardStatsComponent } from './card-stats/card-stats.component';


@NgModule({
  declarations: [
    AppComponent,
    CardComponent,
    PriceGuideComponent,
    SearchCardsComponent,
    TrackedCardsComponent,
    LoginComponent,
    CardStatsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserAnimationsModule,
    MatCardModule,
    FlexLayoutModule,
    MatSidenavModule,
    MatMenuModule,
    MatIconModule,
    MatToolbarModule,
    MatButtonModule,
    MatInputModule,
    MatGridListModule,
    MatSelectModule,
  ],
  providers: [
    HttpClient,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
