import {Component, Input, OnInit} from '@angular/core';
import {Card} from '../../models/card';

@Component({
  selector: 'app-price-guide',
  templateUrl: './price-guide.component.html',
  styleUrls: ['./price-guide.component.scss']
})
export class PriceGuideComponent implements OnInit {

  @Input()
  card: Card;

  constructor() { }

  ngOnInit() {
  }

}
