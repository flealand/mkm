import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Constants} from '../constants';
import {MkmService} from '../mkm.service';
import {Card} from '../models/card';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit, OnChanges {

  @Input()
  public card: Card = null;

  public isChecked: boolean;

  prices: [] = [];

  ngOnInit() {
  }

  constructor(private mkmService: MkmService,) {
  }


  ngOnChanges(changes: SimpleChanges): void {

  }

  trackCard() {
    this.card.product.isTracked = !this.card.product.isTracked;
    this.mkmService.saveCardsToDataBase(this.card).subscribe(res => {
    });
  }

  reloadPriceForCard() {

    this.mkmService.getCardsById(this.card.product.idProduct).subscribe(res => {
      const product = res.product;
      let tmpCard: Card = {
        product: {
          idProduct: product.idProduct,
          name: product.name[Constants.ENG_ID].productName,
          image: product.image,
          expansion: product.expansion,
          rarity: product.rarity,
          priceGuide: product.priceGuide,
          isTracked: product.isTracked,
          website: product.website,
        }
      };
      this.card = tmpCard;
    });
  }

}
