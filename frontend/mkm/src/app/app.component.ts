import {AfterViewInit, Component} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Router} from '@angular/router';
import {Constants} from './constants';
import {MkmService} from './mkm.service';
import {Card} from './models/card';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit{
  routerUrl: string = '';

  constructor(private router: Router) {
    this.routerUrl = router.url;
  }

  ngAfterViewInit(): void {
  }


}
