export interface Card {

  product: {
    idProduct?: any,                          // Product ID
    idMetaproduct?: any,                 // Metaproduct ID
    countReprints?: any,                 // Number of similar products bundled by the metaproduct
    name?: string,
    category?: {                         // Category entity the product belongs to
      idCategory?: any,                  // Category ID
      categoryName?: any               // Category's name
    }
    priceGuide?: {                       // Price guide entity '''(ATTN: not returned for expansion requests)'''
      SELL?: number,                // Average price of articles ever sold of this product
      LOW?: number,                 // Current lowest non-foil price (all conditions)
      LOWEX?: number,               // Current lowest non-foil price (condition EX and better)
      LOWFOIL?: number,             // Current lowest foil price
      AVG?: number,                 // Current average non-foil price of all available articles of this product
    }
    website?: any,                         // URL to the product (relative to MKM's base URL)
    image?: any,                          // Path to the product's image
    expansion?: any,                        // Expansion's name
    expIcon?: any,                         // Index of the expansion icon
    number?: number,                          // Number of product within the expansion (where applicable)
    rarity?: any,                            // Rarity of product (where applicable)
    reprint?: [                          // Reprint entities for each similar product bundled by the metaproduct
      {
        idProduct?: number                // Product ID
        expansion?: string,                 // Expansion's name
        expIcon?: any,                   // Index of the expansion icon
      }
    ],
    countArticles?: number,              // Number of available articles of this product
    countFoils?: number,                  // Number of available articles in foil of this products
    isTracked?: boolean,
  }
}
