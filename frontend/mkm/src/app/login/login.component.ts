import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthService} from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  constructor(private formBuilder: FormBuilder, public authService: AuthService) {
    this.loginForm = this.createFormGroup();
  }

  ngOnInit() {
  }

  onSubmit(formValues: any) {
    this.authService.login(formValues.username, formValues.pw);
    this.loginForm.reset();
  }

  private createFormGroup(): FormGroup {
    return this.formBuilder.group({
      username: [],
      pw: [],
    });
  }

}
