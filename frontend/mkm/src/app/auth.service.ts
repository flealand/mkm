import {HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Router} from '@angular/router';
import * as moment from "moment";
import * as jwt_decode from "jwt-decode";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public static readonly BASE_URL: string = 'http://deapps.ddns.net:3000/';
  constructor(private http: HttpClient, private router: Router) {
  }

  login(username:string, pw:string ) {
    const data = {username: username, pw: pw};
    this.http.post(AuthService.BASE_URL + 'users/login', data).subscribe(authResult => {
      const decodedToken = this.getDecodedAccessToken(authResult['token']);
      localStorage.setItem('token', authResult['token']);
      this.setSession(decodedToken);
      if (this.isLoggedIn()) {
        this.router.navigate(['/search']);
      }
    });
  }

  private setSession(authResult) {
    const expiresAt = moment().add(authResult.exp,'second');
    localStorage.setItem('id_token', authResult.id);
    localStorage.setItem("expires_at", JSON.stringify(expiresAt.valueOf()) );
  }

  public getToken(): string {
    return localStorage.getItem('token');
  }

  logout() {
    localStorage.removeItem("id_token");
    localStorage.removeItem("expires_at");
  }

  public isLoggedIn() {
    const result: boolean =  moment().isBefore(this.getExpiration());
    return result;
  }

  isLoggedOut() {
    return !this.isLoggedIn();
  }

  getDecodedAccessToken(token: string): any {
    try{
      return jwt_decode(token);
    }
    catch(Error){
      return null;
    }
  }

  getExpiration() {
    const expiration = localStorage.getItem("expires_at");
    const expiresAt = JSON.parse(expiration);
    return moment(expiresAt);
  }

}
