var express = require('express');
var router = express.Router();
var unirest = require("unirest");
var queryBuilder = require('../utils/queryBuilder');
const Card = require('../models/card');

router.get('/search', function(req, res, next) {

  const queryParams = { ...req.query };
  if (!req.authenticated || !req.user) {
    res.sendStatus(401);
    return;
  }

  var requestUrl = 'https://api.cardmarket.com/ws/v1.1/output.json/products/' + req.query.name + '/1/1/false';
  var header = queryBuilder.buildAuthHeader(requestUrl);

  var request = unirest('GET', requestUrl)
    .headers({
      'Authorization': header,
    })
    .end(function (response) {
      if (response.error) {
        res.send({result: 'error while getting data from mkm'});
        return;
      }
      const body = JSON.parse(response.raw_body);

      Card.findAll().then(foundCards => {

        for (let i = 0; i < foundCards.length; i++) {
          for (const mkmCard of body.product) {
            if (foundCards[i].idProduct === mkmCard.idProduct) {
              mkmCard['isTracked'] = true;
            }
          }
        }
        res.send(body);
      });
    });
});


router.get('/tracked', function(req, res, next) {

  const queryParams = { ...req.query };
  if (!req.authenticated || !req.user) {
    res.sendStatus(401);
    return;
  }

  Card.findAll().then(dbCards => {
    var productIds = [];

    for (let i = 0; i < dbCards.length; i++) {
      productIds.push(dbCards[i].idProduct);
    }

    var cards = productIds.map(function(id) {
      return new Promise(function(resolve, reject) {

        requestUrl = 'https://api.cardmarket.com/ws/v1.1/output.json/product/' + id;
        header = queryBuilder.buildAuthHeader(requestUrl);

        return unirest('Get', requestUrl).headers({
          'Authorization': header
        }).end(function(data){
            resolve(data);
          });
      });
    });

    Promise.all(cards).then(function(result) {
      var content = result.map(function(card) {
        return card.body;
      });
      res.send(content);
    });
  });
});

router.get('/expansion', function(req, res, next) {

  const queryParams = { ...req.query };
  if (!req.authenticated || !req.user) {
    res.sendStatus(401);
    return;
  }

  var requestUrl = 'https://api.cardmarket.com/ws/v2.0/output.json/games/1/expansions';
  var header = queryBuilder.buildAuthHeader(requestUrl);
  var request = unirest('GET', requestUrl)
    .headers({
      'Authorization': header,
    })
    .end(function (response) {
      if (response.error) {
        res.send({result: 'error while getting data from mkm'});
        return;
      }
      const body = JSON.parse(response.raw_body);
      res.send(body);
    });
});


router.get('/id', function(req, res, next) {

  const queryParams = { ...req.query };
  if (!req.authenticated || !req.user) {
    res.sendStatus(401);
    return;
  }

  var requestUrl = 'https://api.cardmarket.com/ws/v1.1/output.json/product/' + req.query.id;
  var header = queryBuilder.buildAuthHeader(requestUrl);
  var request = unirest('GET', requestUrl)
    .headers({
      'Authorization': header,
    })
    .end(function (response) {
      if (response.error) {
        res.send({result: 'error while getting data from mkm'});
        return;
      }
      const body = JSON.parse(response.raw_body);
      res.send(body);
    });
});


router.get('/expansion/specific', function(req, res, next) {

  const queryParams = { ...req.query };
  if (!req.authenticated || !req.user) {
    res.sendStatus(401);
    return;
  }

  var requestUrl = 'https://api.cardmarket.com/ws/v2.0/output.json/expansions/' + req.query.id + '/singles';
  var header = queryBuilder.buildAuthHeader(requestUrl);
  var request = unirest('GET', requestUrl)
    .headers({
      'Authorization': header,
    })
    .end(function (response) {
      if (response.error) {
        res.send({result: 'error while getting data from mkm'});
        return;
      }
      const body = JSON.parse(response.raw_body);
      res.send(body);
    });
});


router.post('/save', function(req, res, next) {

  const queryParams = { ...req.query };
  if (!req.authenticated || !req.user) {
    res.sendStatus(401);
    return;
  }

  const cardToSave = req.body.product;
  Card.findOne({
    where: {
      idProduct: cardToSave.idProduct
    }
  }).then(found => {
    if (found) {
      Card.destroy({
        where: {
          idProduct: found.idProduct,
        }
      })
    } else {
      // save a card to database
      const card = new Card({
        idProduct: cardToSave.idProduct,
      });

      return card.save().then(success => {
        res.send('saved');
      });
    }
  });
});


module.exports = router;
