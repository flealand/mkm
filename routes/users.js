const express = require('express');
const router = express.Router();
const authController = require('../controllers/auth');

router.post('/login', authController.login);

router.get('/login/check', authController.checkToken);

module.exports = router;
