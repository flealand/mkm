var oAuthSignature = require('oauth-signature');

exports.buildAuthHeader = (requestUrl) => {

  var timeStamp = +new Date();
  const crypto = require('crypto');
  let nonce = crypto.randomBytes(16).toString('base64');

  var httpMethod = 'GET';
  var url = requestUrl;
  var parameters = {
    oauth_consumer_key: 'g8WZ9Q6XArlvsz0i',
    oauth_token: 'zevSa4sHCCaonjRhdklESWHPPYKv6WnR',
    oauth_nonce: nonce,
    oauth_timestamp: timeStamp,
    oauth_signature_method: 'HMAC-SHA1',
    oauth_version: '1.0'
  };
  var consumerSecret = 'c8usYuNW1OrZq1Qr2TUNgEhrEWW7aLZF';
  var tokenSecret = 'rgg60n6oVqY0M8iKWFtAxsCptAhhIoHa';

  var encodedSignature = oAuthSignature.generate(httpMethod, url, parameters, consumerSecret, tokenSecret);
  var encodedUrl = encodeURIComponent(requestUrl);

  return 'OAuth' +
    ' realm="' + encodedUrl + '",oauth_consumer_key="g8WZ9Q6XArlvsz0i",' +
    'oauth_token="zevSa4sHCCaonjRhdklESWHPPYKv6WnR",oauth_signature_method="HMAC-SHA1",' +
    'oauth_timestamp="' + timeStamp + '",oauth_nonce="' + nonce + '",' +
    'oauth_version="1.0",oauth_signature="' + encodedSignature + '"';

};