const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const usersRouter = require('./routes/users');
const cardRouter = require('./routes/cards');
const db = require('./utils/database'),
  sequelize = db.sequelize;

const User = require('./models/user');
const cors = require('cors');
const authController = require('./controllers/auth');

const app = express();

app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// initialize middleware
app.use(authController.verifyToken);
app.use('/users', usersRouter);
app.use('/cards', cardRouter);


module.exports = app;
sequelize.sync({force: false}).then(done => {
 /* User.create({
    pw: '1',
    username: '1',
  });*/
});
